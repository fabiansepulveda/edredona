<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'edredona_site');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'x[h.~hEraFcV6e*j/2f)*[Ok/K|T]5O=rw0b;<JDHA-yeE4XEIW#PcPD=|o574:v');
define('SECURE_AUTH_KEY',  'PLZ)m4~+(|0hr]3swvU~$?t;6*t^=Ysd:}7 H%k$f??TT_9FG,v=Iulf|0y+/]+Y');
define('LOGGED_IN_KEY',    '#hreR+|We4[I(;|%X X6_]T8BC-HFQ)[o-x<Nzp+|0x!)srP*`kx++:I+.&r]M2}');
define('NONCE_KEY',        'EvK$zraCaAPbS,0I0LS~ls/vJb-^XGK.Wx2Y$XOc{Ck<XmegD9_8Ntx9KcKSW{vU');
define('AUTH_SALT',        'YIT5 =@N(Wy&:p8D4[6EWG%m-lv5Sy4-ISXX/lswMJ>]BY|FZ%t|6zL/HZ9`9Ugt');
define('SECURE_AUTH_SALT', '<9F6zTU-L=(S+N81FS+v/QB<~S|$[ngJ|^[f_Z?hwG-7$(@E2|[-0EE.J}vOI;L:');
define('LOGGED_IN_SALT',   'yr~%I}KV )4o>dT{b>ZR!vD_|-+6>I~0r|TcjTU[w6L.38m,@*<Lt5,J;V~Q9p6X');
define('NONCE_SALT',       'NZg:o+-$>+$kJeT%tSb`mW<0$QN$B}c3ne2v6_22G&cN_B@t?PQg3n:($Sn{~^D@');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
define('WP_MEMORY_LIMIT', '256M');
/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
