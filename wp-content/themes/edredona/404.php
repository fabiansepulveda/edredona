<?php get_header();?>
<div class="col-xs-12 content-404 text-center">
    <img src="<?php bloginfo("stylesheet_directory");?>/assets/img/404.png" alt="Error 404" title="Error 404"/>
    <h1>ERROR 404</h1>
    <p><?php echo __('Página no encontrada','edredona'); ?></p> 
</div>
<?php get_footer();?>