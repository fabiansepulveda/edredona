    </div>
    <footer>
        <div class="container">
            <div class="top col-xs-12">
                <div class="share">
                    <div class="fb-like" data-href="https://www.facebook.com/edredonacom/" data-layout="box_count" data-action="like" data-show-faces="false" data-share="true"></div>
                </div>
                <div class="social-net">
                    <ul>
                        <li><a title="Facebook" href="<?php echo theme_get_option( 'facebooklink' ); ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li><a title="Google Plus" href="<?php echo theme_get_option( 'googlelink' ); ?>" target="_blank"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                        <li><a title="Pinterest" href="<?php echo theme_get_option( 'pinterestlink' ); ?>" target="_blank"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
                        <li><a title="Instagram" href="<?php echo theme_get_option( 'instagramlink' ); ?>" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                        <li><a title="Blog Spot" href="<?php echo theme_get_option( 'blogspotlink' ); ?>" target="_blank"><img src="<?php bloginfo('stylesheet_directory');?>/assets/img/blog-icon.png"/></a></li>
                    </ul>
                </div>  
                <div class="gateway">
                    <img class="img-responsive" src="<?php bloginfo('stylesheet_directory');?>/assets/img/pagos.png"/>
                </div>
            </div>
            <div class="clear"></div>
            <div class="mid">
                <?php wp_nav_menu(array('menu_class'=> 'footer-menu','theme_location' => 'menu-footer', 'container' => false));?>  
            </div>
            <div class="clear"></div>
            <div class="bot text-center">
                <p><?php echo __('Todos los Derechos Reservados','edredona'); ?> © <?php echo date('Y'); ?> <b>Edredona</b> - Powered By: <a href="http://dnmk.co/" target="_blank">Dynamik Creative Collective</a></p>
            </div>
        </div>
    </footer>
    </div><!-- .container-->
</div><!-- #general-wrapper -->
<?php wp_footer(); ?>
<script
    type="text/javascript"
    async defer
    src="//assets.pinterest.com/js/pinit.js"
></script>
</body>
</html>