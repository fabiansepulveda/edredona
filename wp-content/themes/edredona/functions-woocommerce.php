<?php 
/** SOPORTE WOOCOMMERCE **/
add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

function woocommerce_template_loop_price() {
    $product = new WC_Product( get_the_ID() );
	echo '<p>'. __('Desde','edredona').' '. $product->get_price_html().'</p>';
}
function woocommerce_excerpt_product() {
    echo '<p>'.substr(get_the_excerpt(), 0,90).'...</p>';
}
function woocommerce_title_product() {
    the_title('<h3>','</h3>'); 
}
function woocommerce_single_product_content(){
    the_content();
}
function woocommerce_single_sharing(){ 
    global $post;
    $thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );?>
    <div class="text-center share-box">
        <a data-pin-do="buttonPin" href="https://www.pinterest.com/pin/create/button/?url=<?php the_permalink() ?>&media=<?php echo esc_attr( $thumbnail_src[0] ) ?>" data-pin-color="red"></a>
        <?php 
        /*echo esc_attr( $thumbnail_src[0] );
        echo "<br/>";
        echo the_permalink(); */?>
        <div class="fb-like" data-href="<?php the_permalink();?>" data-layout="button" data-action="like" data-show-faces="false" data-share="true"></div>
    </div>
<?php }

add_filter('woocommerce_available_variation', function ($value, $object = null, $variation = null) {
    if ($value['price_html'] == '') {
        $value['price_html'] = '<span class="price">' . $variation->get_price_html() . '</span>';
    }
    return $value;
}, 10, 3);

remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );
remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
remove_action('woocommerce_single_variation', 'woocommerce_single_variation_add_to_cart_button', 20);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10);
remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5);
remove_action('woocommerce_single_variation', 'woocommerce_single_variation', 10);

add_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );
add_action('woocommerce_shop_loop_item_title', 'woocommerce_excerpt_product', 20 );
add_action('woocommerce_shop_loop_item_title_price', 'woocommerce_template_loop_price', 20 );
add_action('woocommerce_shop_loop_item_title_price', 'woocommerce_title_product', 10 );
add_action('woocommerce_single_product_summary','woocommerce_single_sharing', 8);
add_action('woocommerce_single_product_summary','woocommerce_single_product_content',9);
add_action('woocommerce_search_product_price','woocommerce_template_single_price',15);

add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );

function woo_remove_product_tabs( $tabs ) {
    
    unset( $tabs['reviews'] ); 			// Remove the reviews tab
    return $tabs;
}

add_filter( 'woocommerce_product_tabs', 'woo_custom_description_tab', 98 );
function woo_custom_description_tab( $tabs ) {
    
   	$tabs['description']['callback'] = 'woo_custom_description_tab_content';
    $tabs['additional_information']['callback'] = 'woo_custom_additional_information_tab_content';
        
    $tabs['description']['title'] = __( 'Características', 'edredona' );
    $tabs['additional_information']['title'] = __( 'Más Información', 'edredona' );
    
	return $tabs;
}

add_filter( 'woocommerce_product_tabs', 'woo_new_product_tab' );
function woo_new_product_tab( $tabs ) {
	$entries = get_post_meta( get_the_ID(), '_pmd_file_complementary', true );
	// Adds the new tab
	
    if( !empty($entries) ){
        $tabs['file_complementary'] = array(
        'title' => __( 'Ficha del producto', 'woocommerce' ),
        'priority' => 50,
        'callback' => 'woo_custom_file_complementary_tab'
        );
    }
    
	return $tabs;

}

function woo_custom_description_tab_content() {
    echo wpautop( get_post_meta( get_the_ID(), '_pmd_tab_caracteristicas', true ) );
}
function woo_custom_additional_information_tab_content() {
     echo wpautop( get_post_meta( get_the_ID(), '_pmd_tab_info', true ) );
}
function woo_custom_file_complementary_tab() {
    echo '<p>Este producto cuenta con los siguientes archivos relacionados:</p>';
    
    $entries = get_post_meta( get_the_ID(), '_pmd_file_complementary', true );
    foreach ( (array) $entries as $key => $entry ) {

        if ( isset( $entry['title_file'] ) )
            $title = esc_html( $entry['title_file'] );

        if ( isset( $entry['file_product'] ) )
            $desc = esc_html( $entry['file_product'] );
        echo '<strong><a target="_blank" href="'. $desc . '">'. $title .'</a></strong><br/>';
    }
}

function wpb_move_comment_field_to_bottom( $fields ) {
    $comment_field = $fields['comment'];
    unset( $fields['comment'] );
    $fields['comment'] = $comment_field;
    return $fields;
}

add_filter( 'comment_form_fields', 'wpb_move_comment_field_to_bottom' );

// Hook in
add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

// Our hooked in function - $fields is passed via the filter!
function custom_override_checkout_fields( $fields ) {
     $fields['billing']['billing_first_name']['placeholder']    = __('Nombre*', 'edredona');
     $fields['billing']['billing_last_name']['placeholder']     = __('Apellidos*', 'edredona');
     $fields['billing']['billing_email']['placeholder']         = __('Email*', 'edredona');
     $fields['billing']['billing_phone']['placeholder']         = __('Teléfono*', 'edredona');
     $fields['billing']['billing_country']['placeholder']       = __('País*', 'edredona');
     $fields['billing']['billing_city']['placeholder']          = __('Ciudad*', 'edredona');
     $fields['billing']['billing_state']['placeholder']         = __('Departamento*', 'edredona');
     $fields['billing']['billing_address_1']['placeholder']     = __('Dirección*', 'edredona');
     $fields['shipping']['shipping_first_name']['placeholder']    = __('Nombre*', 'edredona');
     $fields['shipping']['shipping_last_name']['placeholder']     = __('Apellidos*', 'edredona');
     $fields['shipping']['shipping_email']['placeholder']         = __('Email*', 'edredona');
     $fields['shipping']['shipping_phone']['placeholder']         = __('Teléfono*', 'edredona');
     $fields['shipping']['shipping_country']['placeholder']       = __('País*', 'edredona');
     $fields['shipping']['shipping_city']['placeholder']          = __('Ciudad*', 'edredona');
     $fields['shipping']['shipping_state']['placeholder']         = __('Departamento*', 'edredona');
     $fields['shipping']['shipping_address_1']['placeholder']     = __('Dirección*', 'edredona');
     $fields['account']['account_password']['placeholder']      = __('Password*', 'edredona');
     $fields['order']['order_comments']['placeholder']          = __('Información adicional sobre tu pedido', 'edredona');
     unset($fields['billing']['billing_company']);
     unset($fields['billing']['billing_address_2']);
     unset($fields['billing']['billing_postcode']);
     unset($fields['shipping']['shipping_company']);
     unset($fields['shipping']['shipping_address_2']);
     unset($fields['shipping']['shipping_postcode']);
     $fields['billing']['billing_first_name']['label']          = '';
     $fields['billing']['billing_last_name']['label']           = '';
     $fields['billing']['billing_email']['label']               = '';
     $fields['billing']['billing_phone']['label']               = '';
     $fields['billing']['billing_country']['label']             = '';
     $fields['billing']['billing_city']['label']                = '';
     $fields['billing']['billing_state']['label']               = '';
     $fields['billing']['billing_address_1']['label']           = '';
     $fields['billing']['billing_address_1']['clear']           = true;
     $fields['shipping']['shipping_first_name']['label']          = '';
     $fields['shipping']['shipping_last_name']['label']           = '';
     $fields['shipping']['shipping_email']['label']               = '';
     $fields['shipping']['shipping_phone']['label']               = '';
     $fields['shipping']['shipping_country']['label']             = '';
     $fields['shipping']['shipping_city']['label']                = '';
     $fields['shipping']['shipping_state']['label']               = '';
     $fields['shipping']['shipping_address_1']['label']           = '';
     $fields['account']['account_password']['label']            = '';
     $fields['order']['order_comments']['label']                = '';
     $fields['billing']['billing_first_name']['class']          = array('col-xs-12 col-sm-4');
     $fields['billing']['billing_last_name']['class']           = array('col-xs-12 col-sm-4');
     $fields['billing']['billing_email']['class']               = array('col-xs-12 col-sm-4');
     $fields['billing']['billing_phone']['class']               = array('col-xs-12 col-sm-4');
     $fields['billing']['billing_country']['class']             = array('col-xs-12 col-sm-4');
     $fields['billing']['billing_city']['class']                = array('col-xs-12 col-sm-4');
     $fields['billing']['billing_state']['class']               = array('col-xs-12 col-sm-4');
     $fields['billing']['billing_address_1']['class']           = array('col-xs-12 col-sm-4');
     $fields['shipping']['shipping_first_name']['class']          = array('col-xs-12 col-sm-4');
     $fields['shipping']['shipping_last_name']['class']           = array('col-xs-12 col-sm-4');
     $fields['shipping']['shipping_email']['class']               = array('col-xs-12 col-sm-4');
     $fields['shipping']['shipping_phone']['class']               = array('col-xs-12 col-sm-4');
     $fields['shipping']['shipping_country']['class']             = array('col-xs-12 col-sm-4');
     $fields['shipping']['shipping_city']['class']                = array('col-xs-12 col-sm-4');
     $fields['shipping']['shipping_state']['class']               = array('col-xs-12 col-sm-4');
     $fields['shipping']['shipping_address_1']['class']           = array('col-xs-12 col-sm-4');
     $fields['billing']['billing_email']['clear']                 = false;
     $fields['billing']['billing_last_name']['clear']             = false;
     $fields['billing']['billing_phone']['clear']                 = false;
     $fields['shipping']['shipping_last_name']['clear']           = false;
     $fields['shipping']['shipping_phone']['clear']               = false;
     $fields['shipping']['shipping_country']['clear']             = true;
     return $fields;
}
/*Edit address*/
add_filter( 'woocommerce_billing_fields' , 'custom_override_billing_fields' );
function custom_override_billing_fields( $fields ) {
	unset($fields['billing_company']);
    unset($fields['billing_address_2']);
    unset($fields['billing_postcode']);
    $fields['billing_first_name']['placeholder']     = __('Nombre*', 'edredona');
	$fields['billing_first_name']['label']           = '';
    $fields['billing_first_name']['class']           = array('col-xs-12 col-sm-4');
    
    $fields['billing_last_name']['placeholder']     = __('Apellidos*', 'edredona');
	$fields['billing_last_name']['label']           = '';
    $fields['billing_last_name']['class']           = array('col-xs-12 col-sm-4');
    $fields['billing_last_name']['clear']           = false;
    
	$fields['billing_email']['placeholder']     = __('Email*', 'edredona');
	$fields['billing_email']['label']           = '';
    $fields['billing_email']['class']           = array('col-xs-12 col-sm-4');
    
    $fields['billing_phone']['placeholder']     = __('Teléfono*', 'edredona');
	$fields['billing_phone']['label']           = '';
    $fields['billing_phone']['class']           = array('col-xs-12 col-sm-4');
    $fields['billing_phone']['clear']           = false;
    
    $fields['billing_country']['placeholder']     = __('País*', 'edredona');
	$fields['billing_country']['label']           = '';
    $fields['billing_country']['class']           = array('col-xs-12 col-sm-4');
    
    $fields['billing_address_1']['placeholder']      = __('Dirección*', 'edredona');
	$fields['billing_address_1']['label']           = '';
    $fields['billing_address_1']['class']           = array('col-xs-12 col-sm-4');
    
	$fields['billing_city']['placeholder']      = __('Ciudad*', 'edredona');
	$fields['billing_city']['label']            = '';
    $fields['billing_city']['class']           = array('col-xs-12 col-sm-4');
    
    $fields['billing_state']['placeholder']      = __('Departamento*', 'edredona');
	$fields['billing_state']['label']            = '';
	$fields['billing_state']['class']            = array('col-xs-12 col-sm-4');    
	return $fields;
}
add_filter( 'woocommerce_shipping_fields' , 'custom_override_shipping_fields' );
function custom_override_shipping_fields( $fields ) {
	unset($fields['shipping_company']);
    unset($fields['shipping_address_2']);
    unset($fields['shipping_postcode']);
	$fields['shipping_first_name']['placeholder']     = __('Nombre*', 'edredona');
	$fields['shipping_first_name']['label']           = '';
    $fields['shipping_first_name']['class']           = array('col-xs-12 col-sm-4');
    
    $fields['shipping_last_name']['placeholder']     = __('Apellidos*', 'edredona');
	$fields['shipping_last_name']['label']           = '';
    $fields['shipping_last_name']['class']           = array('col-xs-12 col-sm-4');
    $fields['shipping_last_name']['clear']           = false;
    
	$fields['shipping_email']['placeholder']     = __('Email*', 'edredona');
	$fields['shipping_email']['label']           = '';
    $fields['shipping_email']['class']           = array('col-xs-12 col-sm-4');
    
    $fields['shipping_phone']['placeholder']     = __('Teléfono*', 'edredona');
	$fields['shipping_phone']['label']           = '';
    $fields['shipping_phone']['class']           = array('col-xs-12 col-sm-4');
    $fields['shipping_phone']['clear']           = false;
    
    $fields['shipping_country']['placeholder']     = __('País*', 'edredona');
	$fields['shipping_country']['label']           = '';
    $fields['shipping_country']['class']           = array('col-xs-12 col-sm-4');
    $fields['shipping_country']['clear']           = true;
    
    $fields['shipping_address_1']['placeholder']      = __('Dirección*', 'edredona');
	$fields['shipping_address_1']['label']           = '';
    $fields['shipping_address_1']['class']           = array('col-xs-12 col-sm-4');
    
	$fields['shipping_city']['placeholder']      = __('Ciudad*', 'edredona');
	$fields['shipping_city']['label']            = '';
    $fields['shipping_city']['class']           = array('col-xs-12 col-sm-4');
    
    $fields['shipping_state']['placeholder']      = __('Departamento*', 'edredona');
	$fields['shipping_state']['label']            = '';
	$fields['shipping_state']['class']            = array('col-xs-12 col-sm-4'); 
	return $fields;
}