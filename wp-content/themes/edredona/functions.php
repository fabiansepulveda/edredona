<?php
// Initialize the metabox class
if ( file_exists( dirname( __FILE__ ) . '/lib/cmb2/init.php' ) ) {
	require_once dirname( __FILE__ ) . '/lib/cmb2/init.php';
} elseif ( file_exists( dirname( __FILE__ ) . '/lib/CMB2/init.php' ) ) {
	require_once dirname( __FILE__ ) . '/lib/CMB2/init.php';
}
/* OPCIONES DEL TEMA */
require_once ( get_template_directory() . '/admin/theme-options.php' );
require_once ( get_template_directory() . '/functions-woocommerce.php' );

//secure login para el sitio
/*if ( file_exists( dirname( __FILE__ ) . '/functions/secure.php' ) ) {
    require_once ( get_template_directory() . '/functions/secure.php' );
}*/

add_action( 'send_headers', 'add_header_xframeoptions' );
function add_header_xframeoptions() {
    header( 'X-Frame-Options: SAMEORIGIN' );
}

/* SVG */
function cc_mime_types( $mimes ){
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter( 'upload_mimes', 'cc_mime_types' );

/* Reference Styles and JS */
add_filter( 'wp_enqueue_scripts', 'wpb_adding_scripts', 0 );
function themeslug_enqueue_style() {
	wp_enqueue_style( 'libcss', get_stylesheet_directory_uri() .'/dist/lib.css', false ); 
    wp_enqueue_style( 'main', get_stylesheet_directory_uri() .'/css/styles.min.css', false ); 
    wp_enqueue_style( 'font-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css', false ); 
}
function wpb_adding_scripts() {
    wp_enqueue_script( 'script-lib',get_stylesheet_directory_uri() . '/dist/lib.js','','',false);
    wp_enqueue_script( 'script-flickity',get_stylesheet_directory_uri() . '/js/flickity.pkgd.min.js','','',true);
    wp_enqueue_script( 'script-modernizr',get_stylesheet_directory_uri() . '/dist/modernizr-2.8.3-respond-1.4.2.min.js','','',true);
    wp_enqueue_script( 'script-svg',get_stylesheet_directory_uri() . '/js/svg.min.js','','',true);
    wp_enqueue_script( 'script-waypoints',get_stylesheet_directory_uri() . '/js/jquery.waypoints.min.js','','',true);
    wp_enqueue_script( 'script-main',get_stylesheet_directory_uri() . '/js/script.js','','',true);
    wp_enqueue_script( 'doubleTap', get_stylesheet_directory_uri(). '/dist/jquery.dcd.doubletaptogo.min.js', '', true );
    if (is_page_template( 'template-contact.php' )){
        wp_enqueue_script( 'google-maps','https://maps.googleapis.com/maps/api/js?key=AIzaSyBnSlVgBgZQLHKh_Czm0zf33OZFiErVY_8','','',true);
        wp_enqueue_script( 'lib-mapinfobox-js', get_stylesheet_directory_uri() . '/js/infobox.js','','',true );
        wp_enqueue_script( 'script-map',get_stylesheet_directory_uri() . '/js/map.js','','',true);
        // data to maps the admin
        $latitud_map    = theme_get_option( 'latitud' );
        $longitud_map   = theme_get_option( 'longitud' );
        $latitud_map_1    = theme_get_option( 'latitud_1' );
        $longitud_map_1   = theme_get_option( 'longitud_1' );
        $latitud_map_2    = theme_get_option( 'latitud_2' );
        $longitud_map_2   = theme_get_option( 'longitud_2' );
        $name_site      = get_bloginfo("name");
        $direccion      = theme_get_option( 'direccion' );
        $direccion_1      = theme_get_option( 'direccion_1' );
        $direccion_2      = theme_get_option( 'direccion_2' );
        $data_map       = array(
            'nameSite'      => $name_site,
            'latitudMap'    => $latitud_map,
            'longitudMap'   => $longitud_map,
            'infoBox'       => $direccion,
            'latitudMap_1'    => $latitud_map_1,
            'longitudMap_1'   => $longitud_map_1,
            'infoBox_1'       => $direccion_1,
            'latitudMap_2'    => $latitud_map_2,
            'longitudMap_2'   => $longitud_map_2,
            'infoBox_2'       => $direccion_2
        );
        wp_localize_script( 'script-map', 'object_map', $data_map );
    }
}
add_action( 'wp_enqueue_scripts', 'themeslug_enqueue_style' );
add_action( 'wp_enqueue_scripts', 'wpb_adding_scripts' );  

/*Custom logo in login */
function my_login_logo() { ?>
    <style type="text/css">
        body.login div#login h1 a {
            background-image: url(<?php echo theme_get_option( 'logo' ); ?>);
            padding-bottom: 0px;
            width: 100%;
            height: 150px;
            -webkit-background-size: initial;
            -moz-background-size: initial;
            -o-background-size: initial;
            background-size: initial;
        }
    </style>
<?php
}
add_action( 'login_enqueue_scripts', 'my_login_logo' );
function my_login_logo_url_title() {
    return 'Edredona';
}
add_action('admin_head', 'my_custom_fonts');

function my_custom_fonts() {
  echo '<style>
    .cmb-repeat-group-wrap{
        width: 100%;
        max-width: inherit !important;
    }
    .wide-row{
        width: 50%;
        float: left;
        border: none !important;
    }
    .wide-row .regular-text{
        width: 90%;
    }
  </style>';
}
/* SOPORTE PARA IMAGENES EN EL POST Y REMOVER LOS ATRIBUTOS WIDHT & HEIGHT*/
if (function_exists('add_theme_support')) { 
    add_theme_support('post-thumbnails');
}
add_image_size( 'vertical', 285, 454, array( 'center', 'center' ) );
add_image_size( 'big_thumbanil', 570, 285, array( 'center', 'center' ) );
add_filter( 'post_thumbnail_html', 'remove_width_attribute', 10 );
add_filter( 'image_send_to_editor', 'remove_width_attribute', 10 );
function remove_width_attribute( $html ) {
   $html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
   return $html;
}
/* Registro Menus */
function register_my_menus() {
  register_nav_menus(
    array(
        'main-menu'     => 'Main Menu',
        'menu-right'     => 'Menu Right',
        'menu-footer'     => 'Menu Footer'
    )
  );
}
add_action( 'init', 'register_my_menus' );

/**
    Excerpt Page 
**/
add_action( 'init', 'my_add_excerpts_to_pages' );
function my_add_excerpts_to_pages() {
     add_post_type_support( 'page', 'excerpt' );
}
/**
            PAGINADOR
*/
/* Paginador */

function base_pagination() {
    global $wp_query;

    $big = 999999999; // This needs to be an unlikely integer

    // For more options and info view the docs for paginate_links()
    // http://codex.wordpress.org/Function_Reference/paginate_links
    $paginate_links = paginate_links( array(
        'base'          => str_replace( $big, '%#%', get_pagenum_link($big) ),
        'current'       => max( 1, get_query_var('paged') ),
        'total'         => $wp_query->max_num_pages,
        'mid_size'      => 5,
        'prev_next'     => True,
        'prev_text'     => __('&nbsp;'),
        'next_text'     => __('&nbsp;'),
        'type'          => 'list'
    ) );

    // Display the pagination if more than one page is found
    if ( $paginate_links ) {
        echo '<div class="clear"></div><div id="paginador">';
        echo $paginate_links;
        echo '</div><!--// end .pagination -->';
    }
}

/* Paginador WP_query */
function pagination_query($query = null) {
    if ( !$query ) {
        global $wp_query;
        $query = $wp_query;
    }
     
    $big = 999999999; // need an unlikely integer
 
    $pagination = paginate_links( array(
        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
        'format' => '?paged=%#%',
        'current' => max( 1, get_query_var( 'paged' ) ),
        'total' => $query->max_num_pages,
        'mid_size' => 5,
        'prev_next'    => True,
        'prev_text'    => __('&nbsp;'),
        'next_text'    => __('&nbsp;'),
        'type'         => 'list'
            ) );
    ?>
    
    <div class="clear"></div>
    <div id="paginador">
            <?php echo $pagination; ?>
    </div> 
    <?php
}
/*Form search*/
function add_search_to_wp_menu ( $items, $args ) {
	if( 'menu-right' === $args -> theme_location ) {
$items .= '<li class="menu-item menu-item-search">';
$items .= '<form method="get" class="menu-search-form" action="' . get_bloginfo('home') . '/"><input class="text_input" type="text" value="" placeholder="'.__('Buscar','edredona').'" name="s" id="s" onfocus="if (this.value == \'\') {this.value = \'\';}" onblur="if (this.value == \'\') {this.value = \'\';}" /><input type="submit" class="my-wp-search" id="searchsubmit" value="" /></form>';
$items .= '</li>';
	}
return $items;
}
add_filter('wp_nav_menu_items','add_search_to_wp_menu',10,2);

/*Sidebar*/
add_action( 'widgets_init', 'sidebar_widget' );
function sidebar_widget() {
    register_sidebar( array(
        'name'          => __( 'Filter Category', 'crescent-garden' ),
        'id'            => 'filter-category',
    ));
}
/* Taxonomy */
function my_taxonomy_product() {
  $labels = array(
    'name'              => _x( 'Colección', 'taxonomy general name' , 'edredona'),
    'singular_name'     => _x( 'Colección', 'taxonomy singular name' , 'edredona'),
    'search_items'      => __( 'Search colección' , 'edredona'),
    'all_items'         => __( 'Todas las colecciones' , 'edredona'),
    'parent_item'       => __( 'Parent Category' , 'edredona'),
    'parent_item_colon' => __( 'Parent Category:' , 'edredona'),
    'edit_item'         => __( 'Editar colección' , 'edredona'),
    'update_item'       => __( 'Actualizar colección' , 'edredona'),
    'add_new_item'      => __( 'Añadir nueva colección' , 'edredona'),
    'new_item_name'     => __( 'Nuevo nombre de colección' , 'edredona'),
    'menu_name'         => __( 'Colecciones' , 'edredona'),
  );
  $args = array(
    'labels' => $labels,
    'hierarchical' => true,
  );
  register_taxonomy( 'coleccion', 'product', $args );
}
add_action( 'init', 'my_taxonomy_product', 0 );

function pippin_get_image_id($image_url) {
	global $wpdb;
	$attachment = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid='%s';", $image_url )); 
        return $attachment[0]; 
}

function searchfilter($query) {
    if ($query->is_search && !is_admin() ) {
        $query->set('post_type',array('product'));
    }
return $query;
}

add_filter('pre_get_posts','searchfilter');

//Adding the Open Graph in the Language Attributes
function add_opengraph_doctype( $output ) {
		return $output . ' prefix="og: http://ogp.me/ns#"';
	}
add_filter('language_attributes', 'add_opengraph_doctype');

//Lets add Open Graph Meta Info

function insert_fb_in_head() {
	global $post;
    $default_image = get_bloginfo ('template_url');
    $default_image .= '/assets/img/edredona-fb.jpg';
	if ( is_product()){
        $product    = new WC_Product( get_the_ID() );
        $price      = $product->price;
        $symbol     = get_woocommerce_currency_symbol();
        $price_product = $symbol . number_format($price, 2, ',', '.');
        echo '<meta property="fb:admins" content="360480477304222"/>';
        echo '<meta property="og:locale" content="es_CO">';
        echo '<meta property="og:type" content="product"/>';
        echo '<meta property="og:title" content="' . get_the_title() .' - Desde ' . $price_product .'"/>';
        echo '<meta property="og:description" content="'. $post->post_content .'"/>';
        echo '<meta property="og:url" content="' . get_permalink() . '"/>';
        echo '<meta property="og:site_name" content="'.get_bloginfo ('name').'"/>';
        if(!has_post_thumbnail( $post->ID )) { 
            echo '<meta property="og:image" content="' . $default_image . '"/>';
        }
        else{
            $thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
            echo '<meta property="og:image" content="' . esc_attr( $thumbnail_src[0] ) . '"/>';
        }
    }
    else{
        echo '<meta property="fb:admins" content="360480477304222"/>';
        echo '<meta property="og:locale" content="es_CO">';
        echo '<meta property="og:type" content="article"/>';
        echo '<meta property="og:title" content="' . get_the_title().'"/>';
        //echo '<meta property="og:description" content="'. $post->post_content .'"/>';
        echo '<meta property="og:url" content="' . get_permalink() . '"/>';
        echo '<meta property="og:site_name" content="'.get_bloginfo ('name').'"/>';
        echo '<meta property="og:image" content="' . $default_image . '"/>';
    }
}
add_action( 'wp_head', 'insert_fb_in_head', 5 );