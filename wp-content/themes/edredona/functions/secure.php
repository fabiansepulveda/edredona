<?php
// Simple Query String Login page protection
function example_simple_query_string_protection_for_login_page() {
    $QS = '?edredona=emTh83Ejl18';
    $theRequest = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    $allowed_hosts = array( 'dynamikdemos.a2hosted.com','localhost','www.edredona.com');

        if ( ! isset( $_SERVER['HTTP_HOST']) || ! in_array( $_SERVER['HTTP_HOST'], $allowed_hosts ) ) {
            header( $_SERVER['SERVER_PROTOCOL'] . ' 400 Bad Request' );
            exit;
        }

        if ( site_url('/wp-login.php'.$QS ) == $theRequest ) {
            echo '<script>alert("Acceso Permitido");</script>';
        } else {
            header( 'Location: ' .site_url() );
        }
    }
add_action('login_head', 'example_simple_query_string_protection_for_login_page');
?>