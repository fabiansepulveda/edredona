<!DOCTYPE html>

<html <?php language_attributes(); ?>>

<head>
    <meta charset="UTF-8">
    <title><?php wp_title(); ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--FAVICON-->
    <link rel="icon" href="<?php echo theme_get_option( 'favicon_image' ); ?>" type="image/x-icon">
    <!-- APPLE TOUCH ICON-->
    <link rel="apple-touch-icon-precomposed" href="<?php echo theme_get_option( 'apple_icon_57' ); ?>">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo theme_get_option( 'apple_icon_72' ); ?>">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo theme_get_option( 'apple_icon_114' ); ?>">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo theme_get_option( 'apple_icon_144' ); ?>">
    <link href='https://fonts.googleapis.com/css?family=Fira+Sans:400,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>
    <?php wp_head(); ?>
</head>

<body>
<div id="fb-root"></div>
<script>
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.6";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<div class="general-wrapper">
    <div <?php body_class(); ?>>
        <header>
            <div id="header">
                <div class="container">
                    <div class="top-header">
                        <div class="col-xs-4 col-sm-4 box-left">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <img class="brand-co hidden-xs" src="<?php bloginfo("stylesheet_directory");?>/assets/img/somos-colombia.png" alt="Somos parte de la respuesta" title="Somos parte de la respuesta"/>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-4 text-center logo">
                            <a href="<?php echo icl_get_home_url() ?>" >
                                <img class="img-responsive" src="<?php echo theme_get_option( 'logo' ); ?>" alt="<?php bloginfo( 'name' ); ?>" title="<?php bloginfo( 'name' ); ?>" />
                            </a>
                        </div>
                        <div class="col-xs-4 col-sm-4 car text-right">  
                            <div class="lan-select">
                                <?php do_action('wpml_add_language_selector');?>
                            </div>   
                            <?php if ( is_user_logged_in() ) { 
                                $current_user   = wp_get_current_user();
                                $first_name     = $current_user->user_firstname;
                                $last_name      = $current_user->user_lastname;?>
                                <a class="logout hidden-xs" href="<?php echo wp_logout_url( home_url() ); ?>"><?php echo __('Cerrar sesión','edredona'); ?></a>
                                <a class="my-account" href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>"><?php echo $first_name.' '. $last_name; ?></a>    
                            <?php } else { ?>
                                <a class="my-account" href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>"><?php echo __('Iniciar Sesión', 'edredona'); ?></a>    
                            <?php } ?>
                            <div class="clearfix hidden-xs"></div>      
                            <a class="cart-contents" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'Carrito de compras' ); ?>">
                                <span class="hidden-xs"><?php echo WC()->cart->get_cart_total(); ?></span>
                            </a>
                        </div>                
                    </div>
                    <div class="clear"></div>
                    <nav class="navbar navbar-edredona">
                        <div id="navbar" class="navbar-collapse collapse">
                            <div class="row">
                                <?php wp_nav_menu(array('menu_class'=> 'nav navbar-nav main-menu','theme_location' => 'main-menu', 'container' => false));?>  
                                <?php wp_nav_menu(array('menu_class'=> 'nav navbar-nav navbar-right','theme_location' => 'menu-right', 'container' => false));?>  
                            </div>
                        </div><!--/.nav-collapse -->
                    </nav>
                </div>
            </div>
        </header>
        <div class="clear"></div>
        <div id="main" class="container">