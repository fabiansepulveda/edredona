<?php get_header();?>
<div class="slider">
    <?php echo do_shortcode('[rev_slider alias="main_slider"]'); ?>
</div>
<div class="clear"></div>
<div class="new-products">
    <?php $args = array(
        'post_type' => 'product',
        'posts_per_page' => 5,
        'meta_query' => array(
            array(
                'key' => '_pmd_new_prod',
                'value' => 'on'
            ),
        ),
    );

    $query_new_product = new WP_Query( $args );
    $count = 1;
    if ( $query_new_product->have_posts() ) { ?>
        <div class="col-xs-12 col-sm-12 col-md-3 title text-center">
            <h2><?php echo __('Nuevos productos tienda virtual','edredona'); ?></h2>
        </div>
        <?php while ( $query_new_product->have_posts() ) { 
            $query_new_product->the_post();
            $product = new WC_Product( get_the_ID() );
            if($count == 1 || $count == 5){ ?>
                <div class="item col-xs-12 col-sm-12 col-md-6">
                    <a href="<?php the_permalink();?>">
                        <?php the_post_thumbnail("big_thumbanil");?>
                    </a>
                    <a class="info-box" href="<?php the_permalink();?>">
                        <?php the_title('<h3>','</h3>');?>
                        <p class="price"><?php echo __('Desde','edredona');?> <?php echo $product->get_price_html(); ?></p>
                        <span><?php echo __('Ver producto','edredona'); ?></span>
                    </a>
                </div>
            <?php } if ($count == 2 || $count == 3 || $count == 4){ ?>
                <div class="item col-xs-12 col-sm-4 col-md-3">
                    <a href="<?php the_permalink();?>">
                        <?php the_post_thumbnail("shop_single");?>
                    </a>
                    <a class="info-box" href="<?php the_permalink();?>">
                        <?php the_title('<h3>','</h3>');?>
                        <p class="price"><?php echo __('Desde','edredona');?> <?php echo $product->get_price_html(); ?></p>
                        <span><?php echo __('Ver producto','edredona'); ?></span>
                    </a>
                </div>
            <?php }?>
        <?php $count ++;
        } // end WHILE
        wp_reset_postdata();
    } // end IF
    else { ?>
        <p class="alert"><?php _e( 'Not Found .','edredona'); ?></p>
    <?php } ?>
</div><!-- New products -->
<div class="clear"></div>
<div class="favorite-products">
    <?php $args = array(
        'post_type' => 'product',
        'posts_per_page' => 4,
        'meta_query' => array(
            array(
                'key' => '_pmd_favorite_prod',
                'value' => 'on'
            ),
        ),
    );

    $query_favorite_product = new WP_Query( $args );
    $count = 1;
    if ( $query_favorite_product->have_posts() ) { ?>
        <div class="col-xs-12 col-sm-12 col-md-offset-3 col-md-6 title text-center visible-xs">
            <h2><?php echo __('Nuestros favoritos tienda virtual','edredona'); ?></h2>
        </div>
        <?php while ( $query_favorite_product->have_posts() ) { 
            $query_favorite_product->the_post();
            $product = new WC_Product( get_the_ID() );
            if($count == 1){ ?>
                <div class="item col-xs-12 col-sm-3 top">
                    <a href="<?php the_permalink();?>">
                        <?php the_post_thumbnail("vertical");?>
                    </a>
                    <a class="info-box" href="<?php the_permalink();?>">
                        <span><?php echo __('Ver producto','edredona'); ?></span>
                        <?php the_title('<h3>','</h3>');?>
                        <p class="price"><?php echo __('Desde','edredona');?> <?php echo $product->get_price_html(); ?></p>
                    </a>
                </div>
            <?php }
            if ($count == 2){ ?>
            <div class="col-xs-12 col-sm-6 parent-item">
                <div class="col-xs-12 col-sm-12 col-md-offset-3 col-md-6 title text-center hidden-xs">
                    <h2><?php echo __('Nuestros favoritos tienda virtual','edredona'); ?></h2>
                </div>
                <div class="item col-xs-12 col-sm-6">
                    <a href="<?php the_permalink();?>">
                        <?php the_post_thumbnail("shop_single");?>
                    </a>
                    <a class="info-box" href="<?php the_permalink();?>">
                        <?php the_title('<h3>','</h3>');?>
                        <p class="price"><?php echo __('Desde','edredona');?> <?php echo $product->get_price_html(); ?></p>
                        <span><?php echo __('Ver producto','edredona'); ?></span>
                    </a>
                </div>
            <?php } 
            if ($count == 3){ ?>
                <div class="item col-xs-12 col-sm-6">
                    <a href="<?php the_permalink();?>">
                        <?php the_post_thumbnail("shop_single");?>
                    </a>
                    <a class="info-box" href="<?php the_permalink();?>">
                        <?php the_title('<h3>','</h3>');?>
                        <p class="price"><?php echo __('Desde','edredona');?> <?php echo $product->get_price_html(); ?></p>
                        <span><?php echo __('Ver producto','edredona'); ?></span>
                    </a>
                </div>
            </div>
            <?php } 
            if($count == 4){ ?>
                <div class="item col-xs-12 col-sm-3 top">
                    <a href="<?php the_permalink();?>">
                        <?php the_post_thumbnail("vertical");?>
                    </a>
                    <a class="info-box" href="<?php the_permalink();?>">
                        <span><?php echo __('Ver producto','edredona'); ?></span>
                        <?php the_title('<h3>','</h3>');?>
                        <p class="price"><?php echo __('Desde','edredona');?> <?php echo $product->get_price_html(); ?></p>
                    </a>
                </div>
            <?php } ?>
        <?php $count ++;
        } // end WHILE
        wp_reset_postdata();
    } // end IF
    else { ?>
        <p class="alert"><?php _e( 'Not Found .' ); ?></p>
    <?php } ?>
</div><!-- Favorite products -->
<div class="clear"></div>
<div class="category-box">
    <div class="text-center col-xs-12 col-sm-12 col-md-offset-3 col-md-6">
        <h2><?php echo __('Comprar por categoría', 'edredona'); ?></h2>
    </div>
    <div class="clear"></div>
    <?php $args = array(
        'taxonomy'     => 'product_cat',
        'orderby'      => 'name',
        'show_count'   => 0,
        'pad_counts'   => 0,
        'hierarchical' => 1,
        'title_li'     => '',
        'hide_empty'   => 1
    );
    $all_categories = get_categories( $args );
    foreach ($all_categories as $cat) {
        if($cat->category_parent == 0) {
            $category_id    = $cat->term_id;       
            $thumbnail_id   = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true );
            $image          = wp_get_attachment_url( $thumbnail_id );
            $description    = $cat->description;
            echo '<div class="col-xs-12 col-sm-6 col-md-4 item">';
                echo '<h3>'. $cat->name.'</h3>';
                echo '<a href="'. get_term_link($cat->slug, 'product_cat') .'"><img class="img-responsive" src="' . $image . '" alt="" /></a>';
                echo '<a class="item-hover" href="'. get_term_link($cat->slug, 'product_cat') .'">';
                    echo '<p><b>'. $cat->name.'</b>';
                    echo $description .'<span>'. __('Ver categoria','edredona').'</span></p>';
                echo '</a>';
            echo '</div>';

            $args2 = array(
                    'taxonomy'     => 'taxonomy',
                    'child_of'     => 0,
                    'parent'       => $category_id,
                    'orderby'      => 'name',
                    'show_count'   => 0,
                    'pad_counts'   => 0,
                    'hierarchical' => 1,
                    'title_li'     => '',
                    'hide_empty'   => 1
            );
            $sub_cats = get_categories( $args2 );
            if($sub_cats) {
                foreach($sub_cats as $sub_category) {
                    echo  $sub_category->name ;
                }   
            }
        }       
    }?>
</div>
<?php get_footer();?>