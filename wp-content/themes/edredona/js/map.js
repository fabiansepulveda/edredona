var Lat = Number(object_map.latitudMap);
var Lng = Number(object_map.longitudMap);

var Lat_1 = Number(object_map.latitudMap_1);
var Lng_1 = Number(object_map.longitudMap_1);

var Lat_2 = Number(object_map.latitudMap_2);
var Lng_2 = Number(object_map.longitudMap_2);

var myLatLng = {lat: Lat, lng: Lng};

function initMap() {
  var map = new google.maps.Map(document.getElementById('map-canvas'), {
    zoom: 11,
    center: {lat: 4.716386, lng: -74.181767},
    draggable: true,
  	zoomControl: true, 
  	scrollwheel: true, 
  	disableDoubleClickZoom: true,
    styles: [{"featureType":"landscape","stylers":[{"saturation":-100},{"lightness":65},{"visibility":"on"}]},{"featureType":"poi","stylers":[{"saturation":-100},{"lightness":51},{"visibility":"simplified"}]},{"featureType":"road.highway","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"road.arterial","stylers":[{"saturation":-100},{"lightness":30},{"visibility":"on"}]},{"featureType":"road.local","stylers":[{"saturation":-100},{"lightness":40},{"visibility":"on"}]},{"featureType":"transit","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"administrative.province","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":-25},{"saturation":-100}]},{"featureType":"water","elementType":"geometry","stylers":[{"hue":"#ffff00"},{"lightness":-25},{"saturation":-97}]}]
  });

  function initMarkers(map, markerData) {
    var newMarkers = [],
    marker;
    for(var i=0; i<markerData.length; i++) {
      var contentString = '<div class="content-infobox"><p>'+
      markerData[i].content +
      '</p></div>';
      marker = new google.maps.Marker({
        position:   markerData[i].latLng,
        map:        map,
        icon:       image,
        title:      object_map.nameSite
      }),
      infoboxOptions = {
        content: contentString,
        boxClass : "infoBox",
        alignBottom: true,
        disableAutoPan: false,
        maxWidth: 0,
        pixelOffset: new google.maps.Size(-100,-30),
        zIndex: null,
        closeBoxMargin: "0px",
        closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
        infoBoxClearance: new google.maps.Size(1, 1)
      };
      newMarkers.push(marker);
      newMarkers[i].infobox = new InfoBox(infoboxOptions);
      newMarkers[i].infobox.open(map, marker);
      google.maps.event.addListener(marker, 'click', (function(marker, i) {
          return function() {
              newMarkers[i].infobox.open(map, this);
              map.panTo(markerData[i].latLng);
          }
      })(marker, i));
    }
    return newMarkers;
  }

  /*var contentString = '<div class="content-infobox"><p>'+
      object_map.infoBox +
      '</p></div>';*/

  /*var infowindow = new InfoBox({
    content: contentString,
    boxClass : "infoBox",
    alignBottom: true,
    disableAutoPan: false,
    maxWidth: 0,
    pixelOffset: new google.maps.Size(-100,-30),
    zIndex: null,
    closeBoxMargin: "0px",
    closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
    infoBoxClearance: new google.maps.Size(1, 1)
  });*/

  var image = {
    url: '../wp-content/themes/edredona/assets/img/icon-map.png',
    size: new google.maps.Size(14, 20),
    origin: new google.maps.Point(0,0),
    anchor: new google.maps.Point(0, 20)
  };
  
  /*var marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    icon: image,
    title: object_map.nameSite
  });*/
  /*marker.addListener('click', function() {
    infowindow.open(map, marker);
  });
  google.maps.event.addListener(marker, 'click', function() {
    infowindow.open(map,marker);
  });
  infowindow.open(map,marker);*/
  markers = initMarkers(map, [
        { latLng: new google.maps.LatLng(Lat_1, Lng_1), content: object_map.infoBox_1 },
        { latLng: new google.maps.LatLng(Lat_2, Lng_2), content: object_map.infoBox_2 },
        { latLng: new google.maps.LatLng(Lat, Lng), content: object_map.infoBox }
    ]);
}

google.maps.event.addDomListener(window, 'load', initMap);
google.maps.event.addDomListener(window, "resize", initMap);