jQuery(function(){
    jQuery( 'body' )
    .on( 'updated_checkout', function() {
          usingGateway();
          $(window).resize(function(){
            usingGateway();
        });
    });
    $(function () {
        $('#menu-menu-principal').doubleTapToGo();
    });
});
function usingGateway(){
    var width_1 = $(window).width() - ($('#payment').offset().left + $('#payment').width());
    var b = -width_1; 
    //console.log(b);
    $("#containerWidth").width($(window).width()).css('margin-left',b + 'px');
}
$(document).ready(function(){
    var waypoint = new Waypoint({
        element: document.getElementById('main'),
        handler: function(direction) {
            $('#header').toggleClass('menu-fixed', direction === 'down');
        }
    });
});