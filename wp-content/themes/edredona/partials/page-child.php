<div class="child-page">
    <div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-10">
        <div class="sidebar col-xs-12 col-sm-2 col-md-2">
            <?php 
            $parent     = $post->post_parent;
            $currentId  = $post-> ID;
            $args = array(
                'post_type'      => 'page',
                'posts_per_page' => -1,
                'post_parent'    => $parent,
                'order'          => 'ASC',
                'orderby'        => 'menu_order'
            );
            $parent = new WP_Query( $args );
            if ( $parent -> have_posts() ) {
                while ( $parent -> have_posts() ) { $parent -> the_post(); 
                    $itemId = $post->ID;
                    if($currentId == $itemId){ ?>
                        <div class="item current">
                            <?php the_post_thumbnail('full', array( 'class' => "svg_featured"));?>
                            <?php the_title('<h2>','</h2>'); ?>
                        </div>
                    <?php }
                    else { ?>
                        <div class="item">
                            <a href="<?php the_permalink(); ?>">
                                <?php the_post_thumbnail('full', array( 'class' => "svg_featured"));?>
                                <?php the_title('<h2>','</h2>'); ?>
                            </a>
                        </div>
                    <?php }?>
                <?php } // end while
            } // end if?>        
        </div>
        <div class="wrapper-content col-xs-12 col-sm-10 col-md-10">
            <?php if ( have_posts() ) {
                while ( have_posts() ) {
                    the_post();
                    echo '<div class="row"><div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-11 content-info">';
                    the_content();
                    echo '</div></div>';
                } // end while
            } // end if ?>
        </diV>
    </div>
</div>