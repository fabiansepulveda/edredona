<?php if(is_cart() || is_checkout() || is_account_page()){ ?>
    <?php  //echo 'default';
    //the_title('<h1>','</h1>');
    if ( have_posts() ) {
        while ( have_posts() ) { the_post(); 	
            the_content(); 
        } // end while
} // end if?>
<?php } else { ?>
    <div class="default-page">
        <div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-10">
            <?php  //echo 'default';
            if ( have_posts() ) {
                while ( have_posts() ) { the_post(); 	?>
                    <?php the_title('<h1>','</h1>'); ?>
                    <div class="content-info">
                        <?php the_content(); ?>
                    </div>
                <?php } // end while
            } // end if?>
        </div>
    </div>
<?php } ?>