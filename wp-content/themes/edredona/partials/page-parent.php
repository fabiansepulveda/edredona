<div class="parent-page">
    <div class="text-center">
        <?php the_title('<h1>','</h1>'); ?>
    </div>
    <?php
    $args = array(
        'post_type'      => 'page',
        'posts_per_page' => -1,
        'post_parent'    => $post->ID,
        'order'          => 'ASC',
        'orderby'        => 'menu_order'
    );
    $parent = new WP_Query( $args );
    if ( $parent -> have_posts() ) {
        echo '<div class="col-xs-12 col-sm-12 col-md-offset-2 col-md-8">';
        while ( $parent -> have_posts() ) {
            $parent -> the_post(); ?>
            <div class="col-xs-12 col-sm-4 col-md-4 item">
                <a href="<?php the_permalink(); ?>">
                    <?php the_post_thumbnail('full', array( 'class' => "svg_featured"));?>
                    <?php the_title('<h2>','</h2>'); ?>
                    <span><?php echo __('Ver más','edredona'); ?></span>
                </a>
            </div>
        <?php } // end while
        echo '</div>';
    } // end if
    ?>
</div>