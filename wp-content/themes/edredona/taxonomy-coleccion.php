<?php get_header();
global $post;
$term = $wp_query->get_queried_object();
$title = $term->name;
$idcate = $term->term_id;
$currentterm = $wp_query->queried_object->slug;
$tax = 'coleccion';
$meta_key = 'img_coleccion';
$image = Taxonomy_MetaData::get( $tax, $idcate , $meta_key ); ?>

<div class="wrapper-banner">
    <img src="<?php echo $image; ?>" alt="<?php echo $title; ?>" title="<?php echo $title; ?>">
    <?php if(ICL_LANGUAGE_CODE =='es'){ ?>
        <h1><?php echo __('Colección','edredona');?> <?php echo $title; ?></h1>
    <?php } elseif(ICL_LANGUAGE_CODE =='en') { ?>
        <h1><?php echo $title; ?> <?php echo __('Colección','edredona');?></h1>
    <?php } ?>
</div>
<div class="clear"></div>
<div class="products">
    <?php
    $args = array(
        'post_type' 		=> 'product',
        'coleccion'		=> $currentterm,
        'posts_per_page' 	=> -1
    );  

    $query_coleccion = new WP_Query( $args );

    if ( $query_coleccion->have_posts() ) : while ( $query_coleccion->have_posts() ) : $query_coleccion->the_post();
        wc_get_template_part( 'content', 'product-coleccion' ); 
    endwhile;
        wp_reset_postdata();
    else : ?>
        <p><?php _e( 'Not found.' ); ?></p>
    <?php endif; ?>
</div>
<?php get_footer();?>