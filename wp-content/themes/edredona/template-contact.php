<?php /* Template Name: Map Template */ ?>
<?php get_header();?>
<div class="wrapper-main">
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-md-8 map">
			<div id="map-canvas" style="width:100%; height:420px;"></div>
			<div class="row">
				<div class="col-xs-12 col-sm-3">
					<h2><?php echo __('Teléfonos','edredona');?> </h2>
					<p><?php echo theme_get_option( 'telefonos' ); ?></p>	
				</div>
				<div class="col-xs-12 col-sm-3">
					<h2><?php echo __('Bodega Principal','edredona');?> </h2>
					<p><?php echo theme_get_option( 'direccion_2' ); ?></p>
				</div>
				<div class="col-xs-12 col-sm-3">
					<h2><?php echo __('Sede Madrid','edredona');?></h2>
					<p><?php echo theme_get_option( 'direccion_1' ); ?></p>
				</div>
				<div class="col-xs-12 col-sm-3">
					<h2><?php echo __('Sede Bogotá','edredona'); ?></h2>
					<p><?php echo theme_get_option( 'direccion' ); ?></p>
				</div>
			</div>
		</div>
		<?php 
		if ( have_posts() ) {
			echo '<div class="col-xs-12 col-sm-6 col-md-4 content-info">';
			if ( function_exists('yoast_breadcrumb') ) {
				yoast_breadcrumb('<p id="breadcrumbs">','</p>');
			}
			while ( have_posts() ) {
				the_post(); 	
				the_title('<h1>','</h1>');	
				the_content();
			} // end while
			echo '</div>';
		} // end if
		?>
	</div>
</div>
<?php get_footer();?>