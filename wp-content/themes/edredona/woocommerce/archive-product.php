<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>

<div class="wrapper-shop">
	<div class="row">
		<div class="col-xs-12 col-sm-8 slider">
			<?php echo do_shortcode('[rev_slider alias="tienda"]'); ?>
		</div>
		<div class="col-xs-12 col-sm-4 coleccion-box">
			<div class="gallery js-flickity" data-flickity-options='{ "pageDots": false }'>
				<?php $args = array(
					'taxonomy'     => 'coleccion',
					'orderby'      => 'name',
					'show_count'   => 0,
					'pad_counts'   => 0,
					'hierarchical' => 1,
					'title_li'     => '',
					'hide_empty'   => 0
				);
				$all_categories = get_categories( $args );
				foreach ($all_categories as $cat) {
					if($cat->category_parent == 0) {
						$category_id    = $cat->term_id;       
						$tax 			= 'coleccion';
						$meta_key 		= 'img_coleccion';
						$image 			= Taxonomy_MetaData::get( $tax, $category_id , $meta_key );
						$image_id 		= pippin_get_image_id($image);
						$image_thumb 	= wp_get_attachment_image_src($image_id, 'thumbnail');
						echo '<a href="'. get_term_link($cat->slug, 'coleccion') .'" class="item">';
							if(ICL_LANGUAGE_CODE =='es'){
								echo '<h3><span>'.__('Colección','edredona').'</span><br/>'. $cat->name.'</h3>';
							}
							elseif(ICL_LANGUAGE_CODE =='en') {
								echo '<h3>'. $cat->name.'<br/><span>'.__('Colección','edredona').'</span></h3>';
							}
							echo '<img src="'. $image_thumb[0] .'"/>';
						echo '</a>';

						$args2 = array(
								'taxonomy'     => 'taxonomy',
								'child_of'     => 0,
								'parent'       => $category_id,
								'orderby'      => 'name',
								'show_count'   => 0,
								'pad_counts'   => 0,
								'hierarchical' => 1,
								'title_li'     => '',
								'hide_empty'   => 0
						);
						$sub_cats = get_categories( $args2 );
						if($sub_cats) {
							foreach($sub_cats as $sub_category) {
								echo  $sub_category->name ;
							}   
						}
					}       
				}?>
			</div>
		</div>		
	</div>
	<div class="clear"></div>
	<div class="category-shop">
		<div class="row">
			<div class="text-center">
				<h2><?php echo __('Categorías','edredona'); ?></h2>
			</div>
			<?php $args = array(
				'taxonomy'     => 'product_cat',
				'orderby'      => 'name',
				'show_count'   => 0,
				'pad_counts'   => 0,
				'hierarchical' => 1,
				'title_li'     => '',
				'hide_empty'   => 1
			);
			$all_categories = get_categories( $args );
			foreach ($all_categories as $cat) {
				if($cat->category_parent == 0) {
					$category_id    = $cat->term_id;       
					$thumbnail_id   = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true );
					$image          = wp_get_attachment_url( $thumbnail_id );
					$description    = $cat->description;
					echo '<div class="col-xs-12 col-sm-6 col-md-4 item">';
						echo '<h3>'. $cat->name.'</h3>';
						echo '<a href="'. get_term_link($cat->slug, 'product_cat') .'"><img class="img-responsive" src="' . $image . '" alt="" /></a>';
						echo '<a class="item-hover" href="'. get_term_link($cat->slug, 'product_cat') .'">';
							echo '<p><b>'. $cat->name.'</b>';
							echo $description .'<span>Ver categoria</span></p>';
						echo '</a>';
					echo '</div>';

					$args2 = array(
							'taxonomy'     => 'taxonomy',
							'child_of'     => 0,
							'parent'       => $category_id,
							'orderby'      => 'name',
							'show_count'   => 0,
							'pad_counts'   => 0,
							'hierarchical' => 1,
							'title_li'     => '',
							'hide_empty'   => 1
					);
					$sub_cats = get_categories( $args2 );
					if($sub_cats) {
						foreach($sub_cats as $sub_category) {
							echo  $sub_category->name ;
						}   
					}
				}       
			}?>
		</div>
	</div>
</div>

<?php get_footer( 'shop' ); ?>