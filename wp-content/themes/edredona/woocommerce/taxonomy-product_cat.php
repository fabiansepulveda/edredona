<?php
/**
 * The Template for displaying products in a product category. Simply includes the archive template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/taxonomy-product_cat.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>
<?php global $wp_query;
	global $post;
    $term = $wp_query->get_queried_object();
    $title = $term->name;
    $idcate = $term->term_id;
    $currentterm = $wp_query->queried_object->slug;
    $tax = 'product_cat';
    $meta_key = 'rs_category';
    $image = Taxonomy_MetaData::get( $tax, $idcate , $meta_key );
?>
	<div class="wrapper-info">
		<div class="top-page">
			<div class="col-xs-12 col-sm-3 title">
				<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
					<h1 class="page-title"><?php woocommerce_page_title(); ?></h1>
				<?php endif; ?>
			</div>
			<div class="col-xs-12 col-sm-9 slider">
				<div class="row">
					<?php woocommerce_breadcrumb(); ?>
					<div class="slider-category">
						<?php echo do_shortcode ( $image ); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="clear"></div>
		<div class="col-xs-12 col-sm-3 sidebar">
			<?php if ( is_active_sidebar( 'filter-category' ) ) { ?>
			<div class="filter">
				<p><?php echo __('Filtrar por','edredona'); ?></p>
				<ul>
					<?php dynamic_sidebar( 'filter-category' ); ?>
				</ul>
			</div>
			<?php } ?>
		</div>
		<div class="col-xs-12 col-sm-9 content-info">
			<div class="row">
				<?php
					/**
					* woocommerce_before_main_content hook.
					*
					* @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
					* @hooked woocommerce_breadcrumb - 20
					*/
					do_action( 'woocommerce_before_main_content' );
				?>

				<?php
					/**
					* woocommerce_archive_description hook.
					*
					* @hooked woocommerce_taxonomy_archive_description - 10
					* @hooked woocommerce_product_archive_description - 10
					*/
					//do_action( 'woocommerce_archive_description' );
				?>

				<?php if ( have_posts() ) : ?>

					<?php
						/**
						* woocommerce_before_shop_loop hook.
						*
						* @hooked woocommerce_result_count - 20
						* @hooked woocommerce_catalog_ordering - 30
						*/
						do_action( 'woocommerce_before_shop_loop' );
					?>
					<div class="clear"></div>
					<?php woocommerce_product_subcategories(array('before' => '<div class="text-center"><ul class="subcategory">', 'after' => '</ul></div>')); ?>
					<div class="clear"></div>
					<?php woocommerce_product_loop_start(); ?>					

						<?php while ( have_posts() ) : the_post(); ?>

							<?php wc_get_template_part( 'content', 'product' ); ?>

						<?php endwhile; // end of the loop. ?>

					<?php woocommerce_product_loop_end(); ?>

					<?php
						/**
						* woocommerce_after_shop_loop hook.
						*
						* @hooked woocommerce_pagination - 10
						*/
						do_action( 'woocommerce_after_shop_loop' );
					?>

					<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

						<?php wc_get_template( 'loop/no-products-found.php' ); ?>

					<?php endif; ?>

					<?php
						/**
						* woocommerce_after_main_content hook.
						*
						* @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
						*/
						do_action( 'woocommerce_after_main_content' );
					?>
				</div>
			</div><!-- / content-info-->
		</div><!-- wrapper-info -->
	<?php
		/**
		 * woocommerce_sidebar hook.
		 *
		 * @hooked woocommerce_get_sidebar - 10
		 */
		//do_action( 'woocommerce_sidebar' );
	?>

<?php get_footer( 'shop' ); ?>
